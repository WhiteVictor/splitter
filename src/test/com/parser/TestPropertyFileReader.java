package test.com.parser;

import com.splitter.io.PropertyFileReader;
import org.junit.Test;

public class TestPropertyFileReader {

    @Test
    public void itShouldReadValueFromThePropertyFile() {
        PropertyFileReader reader = new PropertyFileReader("config.properties");
        System.out.println(reader.readProperty("prop2"));
    }
}
