package test.com.parser;

import com.splitter.parser.CustomParser;
import com.splitter.parser.ParserObject;
import com.splitter.parser.ParserProperty;
import org.junit.Test;

import java.util.List;

public class TestCustomParser {

    private static final String TEST_STRING = "[\n" +
            "\t\"imageUrl\" = \"https://yabila.com/java.jpg\",\n" +
            "\t\"parts\" = 6\n" +
            "]";

    @Test
    public void itShouldPrintAllChars()  {
        CustomParser parser = new CustomParser(TEST_STRING);
        List<ParserObject> parsedObjList = parser.parse();
        for (ParserProperty prop: parsedObjList.get(0).getProperties()) {
            System.out.println(String.format("\nProp Key: %s\nProp Val: %s", prop.getPropName(), prop.getPropValue()));
        }
        System.out.println(parsedObjList.get(0).getProperties().size());
    }
}
