package test.com.parser;

import com.splitter.io.PropertyFileWriter;
import org.junit.Test;

import java.io.IOException;

public class TestPropertyWriter {

    @Test
    public void itShouldCreateNewFileAndAppendContentToIt() throws IOException {
        PropertyFileWriter propFileWriter = new PropertyFileWriter();
        propFileWriter.addPropertyToExistingFile("prop1", "This is prop one");
        propFileWriter.addPropertyToExistingFile("prop2", "This is prop two");
    }

    @Test
    public void itShouldCreateNewFileAndAddPropertyToIt() {
        PropertyFileWriter propFileWriter = new PropertyFileWriter();
        try {
            propFileWriter.addPropertyToNewFile("prop1", "This is prop one");
            propFileWriter.addPropertyToNewFile("prop2", "This is prop two");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
