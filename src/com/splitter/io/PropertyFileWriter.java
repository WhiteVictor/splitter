package com.splitter.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class PropertyFileWriter {

    private String propertyFileName = "config.properties";

    public PropertyFileWriter(){}

    public PropertyFileWriter(String propertyFileName) {
        this.propertyFileName = propertyFileName;
    }

    public String addPropertyToExistingFile(String key, String val) throws IOException {
        File file = new File(propertyFileName);
        if (! file.exists()) {
            file.createNewFile();
        }

        String contentToWrite  = String.format("%s:%s\n", key, val);

        FileWriter  writer = new FileWriter(file, true);
        writer.append(contentToWrite);
        writer.close();

        return contentToWrite;
    }

    public String addPropertyToNewFile(String key, String val) throws IOException {
        File file = new File(propertyFileName);
        if (file.exists()) {
            file.delete();
        }
        return addPropertyToExistingFile(key, val);
    }

    public boolean writeToFile(String contentToWrite) throws IOException {
        FileWriter  writer = new FileWriter(propertyFileName, true);
        writer.append(contentToWrite);
        writer.close();

        return true;
    }

    public boolean writeToFile(String content, boolean append) throws IOException {
        File file = new File(propertyFileName);
        if (!file.getParentFile().exists())
            file.getParentFile().mkdir();
        if (! file.exists())
            file.createNewFile();
        if (append)
            return writeToFile(content);

        file.delete();
        return writeToFile(content);

    }
}
