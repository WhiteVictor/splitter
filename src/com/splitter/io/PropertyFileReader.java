package com.splitter.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileReader {

    Properties properties = new Properties();
    private String propFile = "config.properties";

    public PropertyFileReader(){}

    public PropertyFileReader(String propFile) {
        this.propFile = propFile;
        try {
            properties.load(new FileInputStream(propFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readProperty(String propName) {
        return properties.getProperty(propName);
    }
}
