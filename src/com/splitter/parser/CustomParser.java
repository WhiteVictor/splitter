package com.splitter.parser;

import javax.swing.text.html.parser.Parser;
import java.util.ArrayList;
import java.util.List;

public class CustomParser {

    private static final char CREATE_NEW_OBJECT = '[';
    private static final char ADD_OBJECT_TO_LIST = ']';
    private static final char PROPERTY_KEY_WRAPPER = '"';
    private static final char PROPERTY_KEY_VALUE_SWITCHER = '=';
    private static int READ_PROPERTY_VALUE = 0;
    private static final char PROPERTY_SEPARATOR = ',';
    private static int START_READING_PROPERTY = 0;
    private static String property = "";

    private String formattedText;

    public CustomParser(String formattedText) {
        this.formattedText = formattedText
                .replaceAll("\\s", "")
                .replaceAll(System.lineSeparator(), "");
    }

    /**
     * parses the given custom formatted file into its equivalent objects
     * @return
     */
    public List<ParserObject> parse()  {
        List<ParserObject> parsedObjList = new ArrayList<ParserObject>();
        ParserObject parserObject  = null;
        ParserProperty parserProperty = null;
        char[] chars = formattedText.toCharArray();
        int index = 0;

        for (char c: chars) {
            System.out.print(c);

            /*
             * if conditions are like hooks that occurs
             * in the string parsing.
             * Some hook examples are: [  ] "  ,  =
             */
            if (c == CREATE_NEW_OBJECT) {
                parserObject = new ParserObject();
            }

            else if (c == PROPERTY_KEY_WRAPPER)  {
                if (READ_PROPERTY_VALUE == 0) { // read prop key
                    if (START_READING_PROPERTY == 0) {
                        parserProperty = new ParserProperty();
                        START_READING_PROPERTY = 1;
                    } else if (START_READING_PROPERTY == 1){
                        parserProperty.setPropName(property);
                        property = "";
                        START_READING_PROPERTY = 0;
                    }
                }

                else if (READ_PROPERTY_VALUE == 1) { //  read prop val
                    if (START_READING_PROPERTY == 0) {
                        START_READING_PROPERTY = 1;
                    } else if (START_READING_PROPERTY == 1){
                        if (parserProperty != null)
                            parserProperty.setPropValue(property);
                        property = "";
                        START_READING_PROPERTY = 0;
                    }
                }
            }

            else if (c == ADD_OBJECT_TO_LIST) {
                parserObject.addProperty(parserProperty);
                parsedObjList.add(parserObject);
            }

            // Read property key
            else if (c  == PROPERTY_SEPARATOR) {
                READ_PROPERTY_VALUE = 0;
                parserObject.addProperty(parserProperty);
            }

            //  Read property value
            else if (c == PROPERTY_KEY_VALUE_SWITCHER)  {
                // look into future to see if the next character is a Integer value
                if (Character.isDigit(chars[index+1]))
                    parserProperty.setPropValue(String.valueOf(Character.getNumericValue(chars[index+1])));
                READ_PROPERTY_VALUE = 1;
            }



            else {
                if (START_READING_PROPERTY == 1) {
                    property += c;
                }
            }
            index++;
        }

        return parsedObjList;
    }
}
