package com.splitter.parser;

import java.util.ArrayList;
import java.util.List;

public class ParserObject {
    List<ParserProperty> properties;

    public List<ParserProperty> getProperties() {
        return properties;
    }

    public void setProperties(List<ParserProperty> properties) {
        this.properties = properties;
    }

    public void addProperty(ParserProperty parserProperty) {
        if (properties == null) {
            properties = new ArrayList<>();
        }
        if (parserProperty != null)
            properties.add(parserProperty);
    }
}
