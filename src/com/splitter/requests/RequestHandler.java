package com.splitter.requests;

import sun.misc.Request;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Optional;
import java.util.Scanner;

public class RequestHandler {

    private HttpURLConnection http;

    public RequestHandler(Builder builder) {
        this.http = builder.http;
    }

    public static class Builder {
        private HttpURLConnection http;
        private String url;

        public Builder(String urlString) {
            URL url = null;
            try {
                url = new URL(urlString);
                this.http = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public Builder doOutput(boolean doOutput) {
            this.http.setDoOutput(doOutput);
            return this;
        }

        public Builder instanceFollowRedirects(boolean followRedirects) {
            this.http.setInstanceFollowRedirects(followRedirects);
            return this;
        }

        public Builder requestVerb(String requestVerb) throws ProtocolException {
            this.http.setRequestMethod(requestVerb);
            return this;
        }

        public Builder addRequestProperty(String key, String val) {
            this.http.addRequestProperty(key, val);
            return this;
        }

        public RequestHandler connect() {
            try {
                this.http.connect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new RequestHandler(this);
        }


    }

    public Optional<InputStream> getResponse() {
        InputStream is = null;
        try {
            is = this.http.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.of(is);
    }

    public Optional<String> getResponseBody()  {
        return Optional.of(streamToString(getResponse().get()));
    }

    private static String streamToString(InputStream inputStream) {
        String text = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }
}
