import com.splitter.io.PropertyFileReader;
import com.splitter.io.PropertyFileWriter;
import com.splitter.parser.CustomParser;
import com.splitter.parser.ParserObject;
import com.splitter.parser.ParserProperty;
import com.splitter.requests.RequestHandler;

import java.io.*;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final String URL = "https://yabila.com/image.json";

    public static void main(String[] args) {
        //  Custom Parser Example Use case
        CustomParser parser = new CustomParser(fetchFileContent(URL));

        List<ParserObject> parsedObjectList = parser.parse();

        String customImageFileUrl = parsedObjectList.get(0).getProperties().get(0).getPropValue();
        int parts = Integer.parseInt(parsedObjectList.get(0).getProperties().get(1).getPropValue());

        PropertyFileReader reader = new PropertyFileReader("config.properties");

        String folderName = reader.readProperty("prop");

        String fileContent = fetchFileContent(customImageFileUrl);

        try {
            FileWriter writer = new FileWriter(String.format("%s/newFile.jpeg", folderName));
            writer.write(fileContent);
            writer.close();

            char[] contentArray  = fileContent.toCharArray();
            int partSize = contentArray.length / parts;
            int contentLength  = contentArray.length;

            System.out.println("Content Array Size: " + contentArray.length);
            System.out.println("Part Size: " + partSize);
            System.out.println("Parts: " + parts);

            int start = 0;
            int end = partSize;
            int index = 1;
            int counter = 0;
            while (counter < contentLength) {

                System.out.println("Start: " + start);
                System.out.println("End: " + end);

                String contentPartData = "";
                while (start < end  && start < contentLength) {
                    contentPartData += contentArray[start];
                    start++;
                }

                FileWriter contentWriter = new FileWriter(String.format("%s/file_part_%d.jpeg", folderName, index++));
                contentWriter.write(contentPartData);
                contentWriter.close();

                start = end+1;
                end += partSize;
                counter += partSize;
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (ParserObject obj: parsedObjectList) {
            for (ParserProperty prop: obj.getProperties()){
                System.out.println(String.format("\nProp Key: %s\nProp Value: %s", prop.getPropName(), prop.getPropValue()));
            }
        }
    }

    private static String streamToString(InputStream inputStream) {
        String text = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }

    public static String fetchFileContent(String urlQueryString) {
        String fileContent = null;
        try {

            fileContent = new RequestHandler.Builder(urlQueryString)
                    .doOutput(true)
                    .instanceFollowRedirects(false)
                    .requestVerb("GET")
                    .addRequestProperty("Content-Type", "application/json")
                    .addRequestProperty("charset", "utf-8")
                    .connect()
                    .getResponseBody().get();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return fileContent;
    }
}